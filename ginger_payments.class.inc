<?php

/**
 * This file contains classes for the Ginger Payments module.
 */

/**
 * Ginger Payments payment method controller.
 */
class GingerPaymentsPaymentMethodController extends PaymentMethodController {
  public $payment_method_configuration_form_elements_callback = 'ginger_payments_method_configuration';
  public $payment_configuration_form_elements_callback = 'ginger_payments_configuration';

  /**
   * Class constructor.
   */
  public function __construct() {
    $this->title = t('Ginger Payments');
  }

  /**
   * Implements PaymentMethodController::execute().
   */
  public function execute(Payment $payment) {
    $method_data = $payment->method_data;
    $controller_data = $payment->method->controller_data;

    $client = ginger_payments_get_client($payment);
    if ($client) {
      $return_path = GINGER_PAYMENTS_RETURN_PATH;
      $listener_path = GINGER_PAYMENTS_LISTENER_PATH;
      if (!empty($controller_data['webhook_base_url'])) {
        $return_path = $controller_data['webhook_base_url'] . '/' . $return_path;
        $listener_path = $controller_data['webhook_base_url'] . '/' . $listener_path;
      }

      $redirect_url = url($return_path
                . '/' . $payment->pid, array('absolute' => TRUE));
      $webhook_url = url($listener_path
                . '/' . $payment->pid, array('absolute' => TRUE));

      if ($controller_data['advanced']) {
        $ginger_payment = $client->createOrder(
          100 * $payment->totalAmount(TRUE), // Amount in cents.
          $payment->currency_code,
          $method_data['ginger_payments_method'],
          $method_data['ginger_payments_method_data'],
          $payment->description,
          $payment->pid,
          $redirect_url,
          NULL,
          NULL,
          NULL,
          $webhook_url
        );

        $redirect_url = $ginger_payment->firstTransactionPaymentUrl();
      }
      else {
        // Create a payment without payment method.
        $ginger_payment = $client->createOrder(
          100 * $payment->totalAmount(TRUE), // Amount in cents.
          $payment->currency_code,
          NULL,
          array(),
          $payment->description,
          $payment->pid,
          $redirect_url
        );

        // Redirect to the hosted payment pages.
        $redirect_url = 'https://api.gingerpayments.com/pay/' . $ginger_payment->id() . '/';
      }

      // Context data might not be the best location to store this information
      // because in fact it is not related to the context. We might consider
      // storing it in a separate database table just like the controller data
      // of the payment method. (The method_data in the payment object is
      // currently not stored.)
      $payment->context_data['payment'] = array(
        'id' => $ginger_payment->id(),
      );
      entity_save('payment', $payment);

      drupal_goto($redirect_url);
    }
  }
}

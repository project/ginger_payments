CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
This module provides Ginger Payments (https://www.gingerpayments.com/) integration for the
Payment platform (https://drupal.org/project/payment). Ginger Payments is a Dutch payment
service provider. With this module enabled site owner can enable customers
(more general visitors) to perform payments to the site owners Ginger Payments account.
Depending on this Ginger Payments account customers can pay through iDEAL,
Creditcard, Bancontact and Sofort.
 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/ginger_payments
 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/ginger_payments

REQUIREMENTS
------------
This module requires the following modules:
 * Payment (https://drupal.org/project/payment)
 * Libraries (https://drupal.org/project/libraries)
This module requires the following 3rd party libraries:
 * Ginger Payment PHP Bindings (https://github.com/gingerpayments/ginger-php)
You should have an approved Ginger Payments account (https://www.gingerpayments.com/) to be able
to use this module.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Create sites/all/libraries (if it does not already exist)
 * Put Ginger Payment PHP Bindings (https://github.com/gingerpayments/ginger-php)
   in the libraries folder so that Ginger.php is at the path
   sites/all/libraries/ginger_php/src/Ginger.php
   ('git clone https://github.com/gingerpayments/ginger-php.git ginger_php' from
   within the sites/all/libraries folder)
 * Run 'composer install' in the folder sites/all/libraries/ginger_php to install the dependencies for Ginger Payment PHP Bindings
   (see https://getcomposer.org/download/ on how to install composer if you don't have it yet)

CONFIGURATION
-------------
After installation a payment method for Ginger Payments should be created in the
configuration of the Payment platform.
 * Go to admin/config/services/payment
 * Click on Payment methods
 * Click on Add payment method
 * Click on Ginger Payments in the list of available payment method types
 * Fill in a title and the Ginger Payments API key (which can be found in the dashboard
   of your Ginger Payments account)
 * Click the Save button
 * Important remark: Don't set a webhook URL for the website profile in your Ginger Payments dashboard. The Ginger Payments
   module uses a different webhook for each payment.

MAINTAINERS
-----------
Current maintainers:
 * Rico van de Vin (ricovandevin) - https://www.drupal.org/u/ricovandevin
This project has been sponsored by:
 * Finlet
   Finlet offers consultancy and education for Drupal.
   Visit https://finlet.eu for more information.
